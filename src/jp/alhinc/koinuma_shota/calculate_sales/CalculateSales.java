package jp.alhinc.koinuma_shota.calculate_sales;

//必要なパッケージのインポート
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class CalculateSales {
	public static void main(String[] args) throws IOException {



		/*実行構成でディレクトリ（ファイル）のパス（場所）を指定する
		 *今回はargsのインデックス0を指定（コマンドライン引数の使い方資料で確認）
		 *コマンドライン引数が1つ設定されているかの確認を行う処理
		*/
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		/*①支店情報(支店コード・支店名)を保持したい
		 *②売上情報(支店コード・売上金額)を保持したい
		 *上記2種類の情報を保持するためにMap(HashMap)を宣言する。
		*/

		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();

		/*支店定義ファイルのデータを一行ずつ読み込む。
		 *読み込みには"BufferReader"と"FileReader"を使用する
		*/


		if(!inputFiles(args[0], "branch.lst", "支店", "^[0-9]{3}", branchNames, branchSales)) {
			return;
		}


		/*すべてのファイルを取得する為の変数を用意
		 *複数のファイルを取得するため"files"を使う
		 *用意した"files"に"listFiles()"を利用してargs[0]内のファイルをすべて入れていく
		*/

		File[] files = new File(args[0]).listFiles();

		/*売上ファイルの情報を格納するList(ArrayList)を宣言する
		 *売上ファイルは.redと仕様で決まっているので"redFiles"とする
		*/

		List<File> rcdFiles = new ArrayList<File>();

		/*ファイル名（○○〇.○○）が売上ファイル名（数字8桁+.rcd)に当てはまるかを
		 *ループ処理（for文)を使って確認する。
		 *その際に"isFile"を使ってファイルなのかディレクトリなのかも同時に確認する
		 *当てはまっているものは"redFiles"にの変数の値として追加していく
		 *追加し終わったらループ処理を完了する
		*/

		for (int i = 0; i < files.length; i++) {
			if (files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		// 売上ファイルを保持しているList"rcdFiles"のソートを行う

		Collections.sort(rcdFiles);

		/*ソートされたファイルが連番かを確認する。
		 *"rcdFiles"の中にファイル型のデータが入っている
		 *".getName()"でファイル型のデータのファイル名（String型）を取得
		 *"substring"で頭から8桁取り出して、キャスト
		 *キャストしたものを計算して差が１出なければ連番じゃないと判断できる
		*/

		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));
			if ((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		/*"FileReader"を使って売上ファイルの中身を読み込む（支店定義ファイル読み込みと同じ要領）
		 *例外処理も行う
		*/

		for (int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				/*ファイルの中身は文字列として扱われるのでString型の変数"line2"を準備
				 *"FileReader"で読み込んだ売上ファイルの中身を"readLine"をwhile文を使って1行ずつ読み込む。
				 *読み込んだ内容を入れるためにString型の要素を持つList"sales"を宣言する。
				 *読み込んだ内容"line2"を"sales"に追加していく
				*/

				String line;
				List<String> sales = new ArrayList<String>();
				while ((line = br.readLine()) != null) {
					sales.add(line);
				}

				//読み込んだ内容が売上ファイルのフォーマット通りじゃない場合（2行じゃない）のエラー処理

				if (sales.size() != 2) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}

				//読み込んだ支店コードが支店定義ファイルに定義されているものと違う場合のエラー処理

				if (!branchSales.containsKey(sales.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}

				//読み込んだ内容が数字なのかを確認し、数字でない場合はエラーメッセージを表示

				if (!sales.get(1).matches("[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				/*"Sales"に追加された内容"line2"は文字列の為、売上金額として扱いたい
				 *そのためにはLong型に型変換する必要があるので型変換を行う
				 *型変換を行いたいのは"line2"の2つ目の要素のみ（1番目の要素）
				 *".get()"を使って要素を指定して呼び出しする。
				*/

				long fileSales = Long.parseLong(sales.get(1));

				/*上の処理で"line2"の中の1番目の要素（売上金額の文字列）の型変換が完了
				 *型変換した売上金額(faleSales)を売上金額を扱うためのMap"branchSales"に追加したい
				 *なのでまずは".get()"を使用して"branchSales"を取得する
				 *取得した"branchSales"に"fileSales"に格納されている売上金額を足し算する
				 *足し算した結果は"SaleAmount"に代入される
				*/

				Long saleAmount = branchSales.get(sales.get(0)) + fileSales;

				//売上金額（saleAmount)が11桁以上の場合、仕様と異なる為、その場合のエラー処理を行う

				if (saleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				//"branchSales"に"saleAmount"を追加したいので".put()"を使って追加する

				branchSales.put(sales.get(0), saleAmount);

				//売上ファイル読み込みの例外処理

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return ;
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return ;
					}
				}
			}
		}

		if(!outputFiles(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}

	}

	/*コマンドライン引数で指定されたディレクトリ（args[0])に
	 *支店別集計ファイル(ファイル名は"branch.out")を作成する。
	 *"FileWriter"と"BufferedWriter"を使ってファイルの作成、書き込みを行う
	*/

	public static boolean inputFiles(String path, String fileName, String targetName, String targetFormat, Map<String, String> names, Map<String, Long> sales) {
		BufferedReader br = null;
		try {
			File file = new File(path, fileName);

			//そもそも"branch.lst"という名前のファイルが存在しなかった場合のエラー処理をしておく
			if (!file.exists()) {
				System.out.println(targetName + "定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			//Map(HashMap)に値を保持する。(それぞれのデータ)

			String line;
			while ((line = br.readLine()) != null) {
				String[] items = line.split(",");

				//読み込んだファイルの中身がフォーマット通りじゃないときのエラー処理をする

				if (items.length != 2 || (!items[0].matches(targetFormat))) {
					System.out.println(targetName + "定義ファイルのフォーマットが不正です");
					return false;
				}
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;

	}
	public static boolean outputFiles(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {

		BufferedWriter bw = null;
		try {
			File branchFile = new File(path, fileName);
			FileWriter fw = new FileWriter(branchFile);
			bw = new BufferedWriter(fw);
			for (String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
